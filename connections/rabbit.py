import pika

class Rabbit(object):

    def __init__(self):
        connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        self.channel = connection.channel()

    def send_message(self, message):
        self.channel.basic_publish(exchange='', routing_key='scraper-queue', body=message)
        print('Mensagem enviada para fila')

    def get_message(self):
        method_frame, header_frame, body = self.channel.basic_get(queue='scraper-queue')
        if not method_frame:
            return ''
        else:
            self.channel.basic_ack(delivery_tag=method_frame.delivery_tag)
            return body.decode("utf-8")
