import os
from connections.rabbit import Rabbit
from connections.mongodb import Mongodb
from bs4 import BeautifulSoup
import time

class Scraper(object):

    def start(self, folder=None):
        self.mongodb = Mongodb()
        if folder:
            self.read_files_folder(folder)
        else:
            self.rabbit = Rabbit()
            self.read_files_queue()

    def read_files_folder(self, folder):
        files = os.listdir(folder)

        for file in files:
            file_path = '{}\\{}'.format(folder, file)
            html = self.__get_html_from_file(file_path)
            page_info = self.__get_info_html(html)
            self.mongodb.insert_product(page_info)

    def read_files_queue(self):
        while True:
            file_path = self.rabbit.get_message()
            if file_path:
                print('Scraper: {}'.format(file_path))
                html = self.__get_html_from_file(file_path)
                page_info = self.__get_info_html(html)
                self.mongodb.insert_product(page_info)
            else:
                print('Fila vazia')
                time.sleep(5)

    def __get_html_from_file(self, file_path):
        try:
            with open(file_path, 'r', encoding='utf-8') as f:
                html = f.read()
                return html
        except Exception as exc:
            print('Erro ao ler arquivo {} da pasta: {}'.format(file_path, str(exc)))

    def __get_info_html(self, html):
        try:
            page_info = {}
            soup = BeautifulSoup(html, 'lxml')
            desc = self.__get_desc(soup)
            price = self.__get_price(soup)
            page_info.update(desc)
            page_info.update(price)

            return page_info
        except Exception as exc:
            print('Erro ao ler HTML: {}'.format(str(exc)))

    def __get_desc(self, soup):
        try:
            name = soup.find('b', {'itemprop': 'name'}).text
            code = soup.find('span',{'itemprop': 'productID'}).text

            return {'name': name, 'code': code}
        except Exception as exc:
            print('Erro ao ler desc: {}'.format(str(exc)))

    def __get_price(self, soup):
        try:
            price = soup.find('strong', {'id': 'ctl00_Conteudo_ctl00_precoPorValue'}).text

            return {'price': price}
        except Exception as exc:
            print('Erro ao pegar preco: {}'.format(str(exc)))

if __name__== '__main__':
    Scraper().start()