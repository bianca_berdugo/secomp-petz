import logging
from selenium.webdriver import Firefox
from selenium.webdriver.common.action_chains import ActionChains


class CrawlerSelenium(object):

    def __init__(self):
        self.firefox_binary = "/usr/bin/firefox"
        self.__start_browser()

    def get_products_page(self, link):
        self.driver.get(link)
        if self.__has_error():
            self.driver.quit()
            return None

        print('Pagina acessada com sucesso!')
        return self.__get_href_products()

    def __start_browser(self, counter=0):
        try:
            if counter <= 5:
                self.driver = Firefox(firefox_binary=self.firefox_binary, executable_path="/home/SECOMP2019/secomp-petz/geckodriver")
                #self.__navigate_example()
                print('Browser iniciado!')
            else:
                print('5 tentativas, verificar erro manualmente!')
        except Exception as exc:
            counter += 1
            print('Erro ao iniciar firefox: {}'.format(str(exc)))
            self.__start_browser(counter)

    def __navigate_example(self):
        try:
            self.driver.get('https://www.extra.com.br/')
            menu = self.driver.find_element_by_xpath('//*[@id="navbar-collapse"]/div/nav/ul/li[15]')
            ActionChains(self.driver).move_to_element(menu).perform()
            self.driver.find_element_by_xpath('//*[@id="navbar-collapse"]/div/nav/ul/li[15]/div/div/ul[4]/li[1]/a').click()
            print('Pagina alvo!')
        except Exception as exc:
            print('Erro ao navegar: {}'.format(str(exc)))

    def __has_error(self):
        try:
            self.driver.find_element_by_id('ctl00_Conteudo_ctl02_divBuscaVaziaSuperior')
            print('Mensagem de erro: finalizado processo')
            return True
        except:
            print('Sem mensagem de erro')
            return False

    def __get_href_products(self):
        try:
            products_list = self.driver.find_element_by_css_selector("div[class='lista-produto prateleira']")
            elements_href_list = products_list.find_elements_by_css_selector("a[class='link url']")
            href_list = []

            for href in elements_href_list:
                href_list.append(href.get_attribute('href'))

            return href_list
        except Exception as exc:
            print("Erro ao acessar href produtos: {}".format(str(exc)))
            return []