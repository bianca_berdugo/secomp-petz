from connections.mongodb import Mongodb
import pandas as pd
import nltk
from nltk.corpus import stopwords
from fuzzywuzzy import fuzz

class Nlp(object):


    def __init__(self):
        self.mongo = Mongodb()


    def start(self):
        products = self.mongo.get_all()
        df = pd.DataFrame(products)
        products_name = df['name'].tolist()
        self.__fuzzy_analysis(products_name, 'filhotes')


    def __fuzzy_analysis(self, info_compare, compare):
        compare = self.__remove_stop_words(compare)
        compare = self.__stemmer(compare)

        for info in info_compare:
            info_original = info
            info = self.__remove_stop_words(info)
            info = self.__stemmer(info)

            score = fuzz.token_set_ratio(info, compare)

            if score >= 80:
                print('Produto correto {}: {}'.format(str(score),info_original))


    def __remove_stop_words(self, info):
        stop_words = set(stopwords.words('portuguese'))

        word_tokens = nltk.tokenize.word_tokenize(info)

        filtered_sentence = [w for w in word_tokens if not w in stop_words]

        return ' '.join(filtered_sentence)


    def __stemmer(self, info):
        stemmer = nltk.stem.RSLPStemmer()

        word_tokens = nltk.tokenize.word_tokenize(info)

        stemm_sentence = [stemmer.stem(w) for w in word_tokens]

        return ' '.join(stemm_sentence)


if __name__ == "__main__":
    nltk.download('stopwords')
    nltk.download('punkt')
    nltk.download('rslp')
    Nlp().start()